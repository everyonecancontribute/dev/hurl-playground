# Hurl Playground

## Introduction

Learning playground for [Hurl](https://hurl.dev) HTTP tests integrated into [GitLab CI/CD](https://docs.gitlab.com/ee/ci/), including [JUnit test reports](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html).

Various test use cases are provided in the [use-cases/](use-cases/) directory.

The sections below describe different integration ways.

## Simple CI/CD

A simple CI/CD job that install Hurl before running any scripts can be configured like this:

```yaml
# CI/CD job template 
.hurl-tmpl:
    variables:
        HURL_VERSION: 1.8.0
    before_script:
        - DEBIAN_FRONTEND=noninteractive apt update && apt -y install jq curl ca-certificates
        - curl -LO "https://github.com/Orange-OpenSource/hurl/releases/download/${HURL_VERSION}/hurl_${HURL_VERSION}_amd64.deb" 
        - DEBIAN_FRONTEND=noninteractive apt -y install "./hurl_${HURL_VERSION}_amd64.deb" 

hurl-gitlab-com:
    extends: .hurl-tmpl
    script:
        - echo -e "GET https://gitlab.com\n\nHTTP/1.1 200" | hurl --test --color 
```

## Efficient CI/CD

A more efficient pipeline uses a custom built container image, using the provided [Dockerfile](Dockerfile) and [.gitlab-ci.yml](.gitlab-ci.yml) which builds the container image and pushes it with the `latest` tag into the GitLab container registry. 

```yaml
include:
    - template: Docker.gitlab-ci.yml 

# Change Docker build to manual non-blocking
docker-build:
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: manual 
      allow_failure: true

# Hurl job template 
.hurl-tmpl:
    image: registry.gitlab.com/everyonecancontribute/dev/hurl-playground:latest 
    variables:
        HURL_URL: gitlab.com 

# Hurl jobs that check websites 

hurl-dnsmichi-at:
    extends: .hurl-tmpl
    variables:
        HURL_URL: dnsmichi.at 
    script:
        - echo -e "GET https://${HURL_URL}\n\nHTTP/1.1 200" | hurl --test --color

hurl-opsindev-news:
    extends: .hurl-tmpl
    variables:
        HURL_URL: opsindev.news  
    script:
        - echo -e "GET https://${HURL_URL}\n\nHTTP/2 200" | hurl --test --color 
```

## Configuration in GitLab, JUnit integration

```yaml
# Hurl job template
.hurl-tmpl:
    image: registry.gitlab.com/everyonecancontribute/dev/hurl-playground:latest
    variables:
        HURL_URL: gitlab.com
        HURL_JUNIT_REPORT: hurl_junit_report.xml

# Hurl tests from configuration file, generating JUnit report integration in GitLab CI/CD
hurl-report:
    extends: .hurl-tmpl
    script:
      - hurl --test use-cases/*.hurl --report-junit $HURL_JUNIT_REPORT
      # Hack: Workaround for 'id' instead of 'name' in JUnit report from Hurl. https://gitlab.com/gitlab-org/gitlab/-/issues/299086 
      - sed -i 's/id/name/g' $HURL_JUNIT_REPORT
    artifacts:
      when: always
      paths:
        - $HURL_JUNIT_REPORT
      reports:
        junit: $HURL_JUNIT_REPORT
```
