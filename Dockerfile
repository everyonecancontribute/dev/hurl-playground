# Docker image provided in https://github.com/Orange-OpenSource/hurl/tree/master/contrib/docker does not allow to override the entrypoint for CI/CD, no direct shell access possible.
# Build a custom image instead.

FROM debian:11-slim

ENV DEBIAN_FRONTEND noninteractive

ARG HURL_VERSION=1.8.0

RUN apt update && apt install -y curl jq ca-certificates
RUN curl -LO "https://github.com/Orange-OpenSource/hurl/releases/download/${HURL_VERSION}/hurl_${HURL_VERSION}_amd64.deb"
# Use apt install to determine package dependencies instead of dpkg
RUN apt -y install "./hurl_${HURL_VERSION}_amd64.deb"
RUN rm -rf /var/lib/apt/lists/*

CMD ["hurl"]